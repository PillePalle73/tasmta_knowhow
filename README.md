- flashen:

   * https://github.com/tasmota/tasmotizer

- 192.168.4.1 als Startseite, wenn ganz nackt

- auf Konsole auf der Webpage: feste IP und eigener DNS-Server ( PiHole z.B. )
    * SaveData 1
    * IPAddress1 192.168.178.x zum feste IP setzen
    * IPAddress4 192.168.178.x zum DNS-Server-Setzen
    * SaveData 0

- Look & Feel:

    * https://tasmota.github.io/docs/WebUI/

- Zeitzone auf Berlin:
  TimeSTD 0, 0, 10, 1, 3, 60
  TimeDST 0, 0, 3, 1, 2, 120
  timezone 99
  time

- Projekte:

   * https://nerdiy.de/tasmota-ws2812-lichterkette-zum-selber-bauen/

